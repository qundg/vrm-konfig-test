var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssnano');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync').create();
var minimist = require('minimist');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');


var config = {
    scssFiles: [
        './assets/sass/*',
    ],
    jsFiles: [
        './assets/js/main.js',
    ],
    imgFiles: [
        './assets/img/*',
        './assets/img/**/*'
    ]
};

gulp.task('default', function() {
});

gulp.task('browser-sync', ['sass'], function() {
    browserSync.init({
        proxy: '192.168.13.37'
    });
    gulp.watch("assets/sass/*.scss", ['sass']);
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('sass', function() {
    return gulp.src("assets/sass/*")
        .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(plumber.stop())
        .pipe(cssmin())
        .pipe(gulp.dest("assets/css"))
        .pipe(browserSync.stream());
});

gulp.task('minify-js', function() {
    gulp.src(config.jsFiles)
        .pipe(concat('bundle.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./assets/js/min'));
});

gulp.task('compress-img', function() {
    gulp.src(config.imgFiles)
        .pipe(imagemin())
        .pipe(gulp.dest('./assets/img'));
});


gulp.task('watch', ['minify-js', 'browser-sync', 'sass'], function() {
    gulp.watch(config.scssFiles, ['sass']);
    gulp.watch(config.jsFiles, ['minify-js']);
});

gulp.task('build', [
    'sass',
    'minify-js'
]);
