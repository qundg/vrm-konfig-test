# README
## INSTALLATION
#### node_modules to install
- gulp
- gulp-compass
- gulp-sass
- gulp-uglify
- gulp-concat
- gulp-imagemin
- browser-sync

#### WATCH
Run ```gulp watch``` for developing. SASS, JS will be converted and minified and browsersync starts.

#### BUILD
Run ```gulp build```for building the project. SASS gets converted and minfied. JS gets minfied and images are getting compressed for faster loading times.

