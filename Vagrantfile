# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  # config.ssh.private_key_path = [ '~/.vagrant.d/insecure_private_key', '~/.ssh/id_rsa' ]
  config.ssh.forward_agent = true
  # config.ssh.insert_key = true
  # config.ssh.password = "qundg"

  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/xenial64"
  config.vm.hostname = "maschinsche"
  # config.vm.box_url = "https://maschinsche.qundg.de/maschinsche-php7/"

  # config.vbguest.auto_update = true

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  #config.vm.network "forwarded_port", guest: 80, host: 1337
  #config.vm.network "forwarded_port", guest: 3306, host: 4711

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.13.37"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder ".", "/var/www",
        #type: "nfs"
        owner: "vagrant",
        group: "www-data",
        mount_options: ["dmode=775,fmode=664"]

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
   config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  #   vb.name = "qundg-maschinsche2"
  vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
  vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
   end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.

   config.vm.provision "shell", inline: <<-SHELL
    #! /usr/bin/env bash

    sed -i 's/us.archive/de.archive/g' /etc/apt/sources.list

    SSH_FIX_FILE="/etc/sudoers.d/root_ssh_agent"
    if [ ! -f  $SSH_FIX_FILE ]
        then
        echo "Defaults env_keep += \"SSH_AUTH_SOCK\"" > $SSH_FIX_FILE
        chmod 0440 $SSH_FIX_FILE
    fi
    tput setaf 1
    apt-get -y install language-pack-de >> /var/www/vm_build.log 2>&1
    apt-get -y install unzip >> /var/www/vm_build.log 2>&1

    echo -e "\n--- Guude! Eben geht's los mit der Installiererei vom Maschinsche... ---\n"
    apt-get update >> /var/www/vm_build.log 2>&1

    echo -e "\n--- Nu installiermer den Apatsche unn des PHP ---\n"
    apt-get -y install php apache2 libapache2-mod-php php-curl php-gd php-mysql php-gettext php-mbstring php-zip composer >> /var/www/vm_build.log 2>&1

    rm /var/www/html/index.html >> /var/www/vm_build.log 2>&1
    rm -R  /var/www/html >> /var/www/vm_build.log 2>&1

    echo -e "\n--- mod-rewrite anmache... ---\n"
    a2enmod rewrite >> /var/www/vm_build.log 2>&1

    echo -e "\n--- AllowOverride brauche mer aach... ---\n"
    sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf
    sed -i "s#/var/www/html#/var/www#g" /etc/apache2/sites-available/000-default.conf

    echo -e "\n--- Unn die Fehler vom PHP stelle mer aach noch an ---\n"
    sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/7.0/apache2/php.ini
    sed -i "s/display_errors = .*/display_errors = On/" /etc/php/7.0/apache2/php.ini

    echo -e "\n--- Jetz noch de Apatsche neue stadde ---\n"

    echo "SetEnv WP_ENV development" >> /etc/apache2/apache2.conf

    service apache2 restart >> /var/www/vm_build.log 2>&1

   SHELL
end